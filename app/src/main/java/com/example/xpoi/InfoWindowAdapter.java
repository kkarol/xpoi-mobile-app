package com.example.xpoi;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.xml.sax.XMLReader;

public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private Marker currentMarker;
    private View view;

    public InfoWindowAdapter(Context context) {
        this.context = context.getApplicationContext();
        this.view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.info_window,
                null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {

        if (currentMarker != null
                && currentMarker.isInfoWindowShown()) {
            currentMarker.hideInfoWindow();
            currentMarker.showInfoWindow();
        }
        if(true)
        return null;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v =  inflater.inflate(R.layout.info_window, null);
        String data = (String) marker.getTag();

        TextView textView = (TextView) v.findViewById(R.id.textView);
        Spanned spannedContent = Html.fromHtml("<h1>" + marker.getTitle() + "</h1>" + data, null, new UlTagHandler());
        textView.setText(spannedContent, TextView.BufferType.SPANNABLE);
        //textView.setSpannableFactory();

/*
        WebView wv = (WebView) v.findViewById(R.id.webv);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        wv.loadUrl("https://www.google.com");
        //wv.loadData("<!DOCTYPE html><head></head><body>" + data + "</body></html>", "text/html; charset=utf-8", "UTF-8");
*/
        return v;
    }

    class UlTagHandler implements Html.TagHandler{
        @Override
        public void handleTag(boolean opening, String tag, Editable output,
                              XMLReader xmlReader) {
            //if(tag.equals("ul") && !opening) output.append("\n");
            //if(tag.equals("li") && opening) output.append("\n• ");
            if(tag.equals("li")){
                if(opening){
                    output.append(" • ");
                }
                else{
                    output.append("\n");
                }
            }


        }
    }
}
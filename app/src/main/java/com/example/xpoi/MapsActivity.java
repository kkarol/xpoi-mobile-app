package com.example.xpoi;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import android.text.style.LineBackgroundSpan;
import android.text.style.QuoteSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.Layer;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.FIFOLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.XMLReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private Map<String, List<Marker>> imageMarkers;
    private RequestQueue requestQueue;

    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private final String rootUrl = "http://xpoi.azurewebsites.net"; //"http://192.168.1.134";  // "http://192.168.137.1";
    private final String applicationPath = rootUrl; // rootUrl + "/extended-poi-app/public";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        initImageLoader();
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.empty)        //    Display Stub Image
                .showImageForEmptyUri(R.drawable.empty)    //    If Empty image found
                .cacheInMemory(true)
                .cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        InfoWindowAdapter2 markerInfoWindowAdapter = new InfoWindowAdapter2(getApplicationContext());
        googleMap.setInfoWindowAdapter(markerInfoWindowAdapter);

        googleMap.setOnInfoWindowClickListener(this);


        getReq();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        //Toast.makeText(this, "Gir Forest Clicked!!!!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        //String m = marker.getId();
        // Return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return true;
    }

    private void getReq(){
        final AlertDialog alertDialog = new AlertDialog.Builder(MapsActivity.this).create();
        alertDialog.setTitle("response");

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        requestQueue = Volley.newRequestQueue(this);
        String url = applicationPath + "/rest/last_saved_poi";

        //com.android.volley.toolbox.ImageRequest

        imageMarkers = new HashMap<>(); // String, ArrayList<Marker>

        JsonObjectRequest stringRequest = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray markers = response.getJSONArray("features");
                            LatLng position = null;
                            for(int i = 0; i < markers.length(); i++){
                                JSONObject markerObj = markers.getJSONObject(i);
                                JSONArray coordinates = markerObj.getJSONObject("geometry").getJSONArray("coordinates");
                                JSONObject properties = markerObj.getJSONObject("properties");
                                position = new LatLng(coordinates.getDouble(1), coordinates.getDouble(0));
                                Marker marker = mMap.addMarker(new MarkerOptions().position(position).anchor(0.5f, 0.5f));
                                marker.setTitle(properties.getString("title"));
                                marker.setTag(properties.getJSONArray("images"));
                                marker.setSnippet(properties.getString("description"));
                                if(properties.getString("marker_icon").length() > 0){
                                    String iconUrl = properties.getString("marker_icon");
                                    if(imageMarkers.containsKey(iconUrl)){
                                        imageMarkers.get(iconUrl).add(marker);
                                    }
                                    else{
                                        ArrayList<Marker> al = new ArrayList<>();
                                        al.add(marker);
                                        imageMarkers.put(iconUrl, al);
                                    }
                                }
                            }
                            Listener ls = new ListenerImpl();
                            for (String key : imageMarkers.keySet()) {
                                getImage(rootUrl + key, ls, key);
                            }
                            if(position != null){
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 5));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MapsActivity.this, "Błąd połączenia", Toast.LENGTH_SHORT).show();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    public interface Listener {
        /** Called when a response is received. */
        public void onResponse(Object tag, Bitmap response);
        public void onErrorResponse(Object tag, VolleyError error);
    }

    class ListenerImpl implements Listener{
        @Override
        public void onResponse(Object tag, Bitmap response) {
            for (Marker m : imageMarkers.get((String)tag)) {
                m.setIcon(BitmapDescriptorFactory.fromBitmap(scaleBitmap(response, 48)));
            }
        }

        @Override
        public void onErrorResponse(Object tag, VolleyError error) {

        }
    }

    public void getImage(String url, final Listener listener, final Object tag){
        ImageRequest jsonObjReq = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        listener.onResponse(tag, response);
                    }
                },
                0, // Image width
                0, // Image height
                ImageView.ScaleType.CENTER, // Image scale type
                Bitmap.Config.RGB_565,
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onErrorResponse(tag,error);
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);
        //AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public Bitmap scaleBitmap(Bitmap src, int size_dp){
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        int p = (int) (size_dp * scale + 0.5f);
        return Bitmap.createScaledBitmap(src, p, p, true);
    }

    private void initImageLoader() {
        int memoryCacheSize;
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            int memClass = ((ActivityManager)
                    getSystemService(Context.ACTIVITY_SERVICE))
                    .getMemoryClass();
            memoryCacheSize = (memClass / 8) * 1024 * 1024;
        //} else {
        //    memoryCacheSize = 2 * 1024 * 1024;
        //}

        final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this).threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .memoryCacheSize(memoryCacheSize)
                .memoryCache(new FIFOLimitedMemoryCache(memoryCacheSize-1000000))
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();

        ImageLoader.getInstance().init(config);
    }

    class InfoWindowAdapter2 implements GoogleMap.InfoWindowAdapter {

        private Context context;
        private Marker currentMarker;
        private View view;
        private int imgCount;
        private int callCount;


        public InfoWindowAdapter2(Context context) {
            this.context = context.getApplicationContext();
            this.view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.info_window,
                    null);
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            //return null;
            currentMarker = marker;
            if (currentMarker != null
                    && currentMarker.isInfoWindowShown()) {
                currentMarker.hideInfoWindow();
                currentMarker.showInfoWindow();
            }

            return null;
        }

        private void  getInfoWindowCallback(Marker marker){
            callCount++;
            if(callCount == imgCount){
                getInfoWindow(marker);
            }
        }

        @Override
        public View getInfoContents(final Marker marker) {
            TableLayout imageList = (view.findViewById(R.id.thumbs));

            TextView textView = (TextView) view.findViewById(R.id.textView);
            Spanned spannedContent = Html.fromHtml("<h1>" + marker.getTitle() + "</h1>" + marker.getSnippet(), null, new UlTagHandler());
            Spannable buffer = new SpannableString(spannedContent);
            replaceQuoteSpans(buffer);
            textView.setText(buffer);

            imgCount = 0;
            callCount = 0;
            String url = null;
            JSONArray images = (JSONArray) marker.getTag();
            if(images != null && images.length() > 0){
                ImageView[] imageViews = {view.findViewById(R.id.badge), view.findViewById(R.id.badge2), view.findViewById(R.id.badge3),
                                          view.findViewById(R.id.badge4), view.findViewById(R.id.badge5), view.findViewById(R.id.badge6)};
                try {
                    int viewsCount = imageViews.length;
                    final int imageCount = Math.min(images.length(), viewsCount);
                    imgCount = imageCount;
                    int i;
                    for(i = 0; i < imageCount; i++){
                        url = rootUrl + images.getString(i);
                        imageLoader.displayImage(url, imageViews[i], options,
                                new SimpleImageLoadingListener() {
                                    @Override
                                    public void onLoadingComplete(String imageUri,
                                                                  View view, Bitmap loadedImage) {
                                        super.onLoadingComplete(imageUri, view,
                                                loadedImage);
                                        getInfoWindowCallback(marker);
                                    }
                                });
                        imageViews[i].setVisibility(View.VISIBLE);
                    }

                    for(int j = i; j < viewsCount; j++){
                        imageViews[j].setVisibility(View.GONE);
                    }
                    imageList.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                imageList.setVisibility(View.GONE);
            }
/*
            final ImageView image = (view.findViewById(R.id.badge));

            if (url != null) {
                imageLoader.displayImage(url, image, options,
                        new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingComplete(String imageUri,
                                                          View view, Bitmap loadedImage) {
                                super.onLoadingComplete(imageUri, view,
                                        loadedImage);
                                getInfoWindow(marker);
                            }
                        });
                image.setVisibility(View.VISIBLE);
            } else {
                image.setImageResource(R.mipmap.ic_launcher);
                image.setVisibility(View.GONE);
            }
*/

            return view;


        }

        private void replaceQuoteSpans(Spannable spannable) {
            QuoteSpan[] quoteSpans = spannable.getSpans(0, spannable.length(), QuoteSpan.class);
            for (QuoteSpan quoteSpan : quoteSpans) {
                int start = spannable.getSpanStart(quoteSpan);
                int end = spannable.getSpanEnd(quoteSpan);
                int flags = spannable.getSpanFlags(quoteSpan);
                spannable.removeSpan(quoteSpan);

                spannable.setSpan(new CustomQuoteSpan(
                        ContextCompat.getColor(context, R.color.common_google_signin_btn_text_dark),
                ContextCompat.getColor(context, R.color.quote_strip),
                                10,
                                20),
                        start,
                        end,
                        flags);
            }
        }

        class UlTagHandler implements Html.TagHandler{
            @Override
            public void handleTag(boolean opening, String tag, Editable output,
                                  XMLReader xmlReader) {
                if(tag.equals("li")){
                    if(opening){
                        output.append(" • ");
                    }
                    else{
                        output.append("\n");
                    }
                }


            }
        }

        class CustomQuoteSpan implements LeadingMarginSpan, LineBackgroundSpan {
            private final int backgroundColor;
            private final int stripeColor;
            private final float stripeWidth;
            private final float gap;

            public CustomQuoteSpan(int backgroundColor, int stripeColor, float stripeWidth, float gap) {
                this.backgroundColor = backgroundColor;
                this.stripeColor = stripeColor;
                this.stripeWidth = stripeWidth;
                this.gap = gap;
            }

            @Override
            public int getLeadingMargin(boolean first) {
                return (int) (stripeWidth + gap);
            }

            @Override
            public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom,
                                          CharSequence text, int start, int end, boolean first, Layout layout) {
                Paint.Style style = p.getStyle();
                int paintColor = p.getColor();

                p.setStyle(Paint.Style.FILL);
                p.setColor(stripeColor);

                c.drawRect(x, top, x + dir * stripeWidth, bottom, p);

                p.setStyle(style);
                p.setColor(paintColor);
            }

            @Override
            public void drawBackground(Canvas c, Paint p, int left, int right, int top, int baseline, int bottom, CharSequence text, int start, int end, int lnum) {
                int paintColor = p.getColor();
                p.setColor(backgroundColor);
                c.drawRect(left, top, right, bottom, p);
                p.setColor(paintColor);
            }
        }
    }

}
